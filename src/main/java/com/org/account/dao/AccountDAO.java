package com.org.account.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.org.account.Account;

@Repository
public class AccountDAO {
	private static Map<String, Account> ACCOUNTS = new LinkedHashMap<String, Account>();

	public AccountDAO() {
		// Create Test DATA
		ACCOUNTS.put("Jim", new Account("Jim", 100.0));
		ACCOUNTS.put("Tom", new Account("Tom", 5.1));
		ACCOUNTS.put("Kim", new Account("Kim", 112.43));
		ACCOUNTS.put("Ash", new Account("Ash", 1233.57));
		ACCOUNTS.put("Patty", new Account("Patty", 53451.54));
		ACCOUNTS.put("Julia", new Account("Julia", 1.1));
	}

	public void addNew(Account acc) {
		ACCOUNTS.put(acc.getName(), acc);
	}

	public void saveModification(Account acc) {
		ACCOUNTS.put(acc.getName(), acc);
	}
	
	
	public List<Account> getAll() {
		return new ArrayList<Account>(ACCOUNTS.values());
	}

	public Account getAccount(String name) {
		return ACCOUNTS.get(name);
	}

	public List<Account> getAccounts(List<String> names) {
		return ACCOUNTS.entrySet().stream().filter(val -> {
			if (names.contains(val.getKey())) {
				return true;
			}
			return false;
		}).map(map -> map.getValue()).collect(Collectors.toList());
	}

}
